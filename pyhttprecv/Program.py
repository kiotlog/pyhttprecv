#!/usr/bin/env python3
"""Example microservice for SigFox (HTTP Callback) -> Flask -> PostgreSQL streaming pipeline.
"""

from datetime import datetime
from pprint import pformat

import click

from flask import Flask, request
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import sessionmaker

from Kiotlog.Services.DB import Point
from Kiotlog.Services.Decoder import Decoder

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/<channel>/<application>/devices/<device>/up', methods=['POST'])
def get_data(channel, application, device):

    msg = request.json
    print(f'Received message from {device}.{application}.{channel}')
    print(pformat(msg))

    pg_session = app.config['POSTGRES_SESSION']

    decoder = Decoder(pg_session, channel, application)

    fields = decoder.decode(msg['payload_raw'], device)

    point = Point(
        device_device=device,
        time=datetime.utcfromtimestamp(msg['metadata']['time']),
        flags=msg['metadata'],
        data=fields
    )

    pg_session.add(point)
    pg_session.commit()

    return f'Data for device {device} received.'


@click.command()
@click.option('--host', default='127.0.0.1', help='Bind IP Address (127.0.0.1)')
@click.option('--port', default=5000, help='Bind IP Address (5000)')
@click.option('--pg-user', default='postgres', help='Default PostgreSQL user (postgres).')
@click.option('--pg-pass', default='postgres', help='Default PostgreSQL password (postgres).')
@click.option('--pg-host', default='localhost', help='Host of PostgreSQL server (localhost).')
@click.option('--pg-port', default=5432, help='Port of PostgreSQL server (5432).')
@click.option('--pg-db', default='kiotlog', help='Default PostgreSQL database (kiotlog).')
def main(host, port, pg_user, pg_pass, pg_host, pg_port, pg_db):

    postgres_db = {
        'drivername': 'postgres',
        'username': pg_user,
        'password': pg_pass,
        'host': pg_host,
        'port': pg_port,
        'database': pg_db
    }

    engine = create_engine(URL(**postgres_db), echo=False)
    session_maker = sessionmaker(bind=engine)
    session = session_maker()

    app.config.update(dict(POSTGRES_SESSION=session))
    app.run(host=host, port=port)


if __name__ == '__main__':
    main()

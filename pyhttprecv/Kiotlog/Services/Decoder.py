""" Module Decoder
"""

import base64
import binascii
import struct

from ..Utils.Conversions import convert
from .DB import Device


class Decoder(object):
    """ Decoder Class
    """

    # TODO: remove dependency from SQLAlchemy

    str_to_bytearray_map = {
        'sigfox': lambda data_str: binascii.unhexlify(data_str),
        'lorawan': lambda data_str: base64.b64decode(data_str)
    }

    def __init__(self, db_session, channel, application):
        self.session = db_session
        self.channel = channel
        self.application = application

    def str_to_bytearray(self, data_str):
        return self.str_to_bytearray_map[self.channel](data_str)

    def decode(self, data_str, tx_device):

        # TODO: remove dependency from SQLAlchemy

        device = self.session. \
            query(Device). \
            filter(Device.device == tx_device). \
            one()

        endianness = '>' if device.frame['bigendian'] is True else '<'

        # Get current device's sensors list, sorted by index.
        sensors = sorted(device.sensors, key=lambda x: x.fmt['index'])

        # Recreate format string for pack/unpack from single sensor format character.
        fmt_str = endianness + ''.join([sensor.fmt['fmt_chr'] for sensor in sensors])

        # From string to array of bytes
        data = self.str_to_bytearray(data_str)

        # Unpack data struct
        # TODO: add support for bitfields structs.
        frame = struct.unpack(fmt_str, bytes(data))

        # Helper closure for converting values back.
        def _do_convert(field, sensor):
            convert_fun = sensor.conversion.fun
            max_val = sensor.sensor_type.meta.get('max', 1)
            min_val = sensor.sensor_type.meta.get('min', 0)

            return convert(field, max_val, min_val, convert_fun)

        # Iterate over unpacked fields and sensors and create a dictionary with decoded data
        received_data = {sensor.meta['name']: _do_convert(field, sensor) for field, sensor in zip(frame, sensors)}

        return received_data

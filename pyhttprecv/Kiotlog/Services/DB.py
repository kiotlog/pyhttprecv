# coding: utf-8
from sqlalchemy import Column, ForeignKey, JSON, Text, text
from sqlalchemy.dialects.postgresql.base import UUID, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class Device(Base):
    __tablename__ = 'devices'

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    device = Column(Text, nullable=False, unique=True)
    meta = Column(JSON, nullable=False, server_default=text("'{}'::jsonb"))
    auth = Column(JSON, nullable=False, server_default=text("'{}'::jsonb"))
    frame = Column(JSON, nullable=False, server_default=text("'{\"bigendian\": true, \"bitfields\": false}'::jsonb"))

    points = relationship('Point', back_populates='device')
    sensors = relationship('Sensor', back_populates='device')


class Point(Base):
    __tablename__ = 'points'

    id = Column(UUID, primary_key=True, server_default=text("gen_random_uuid()"))
    device_device = Column(ForeignKey('devices.device', onupdate='CASCADE'), nullable=False)
    time = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))
    flags = Column(JSON, nullable=False, server_default=text("'{}'::jsonb"))
    data = Column(JSON, nullable=False, server_default=text("'{}'::jsonb"))

    device = relationship('Device', back_populates='points')


class SensorType(Base):
    __tablename__ = 'sensor_types'

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    name = Column(Text, nullable=False, unique=True, server_default=text("'generic'::text"))
    meta = Column(JSON, server_default=text("'{}'::jsonb"))
    type = Column(Text, nullable=False, server_default=text("'generic'::text"))

    sensors = relationship('Sensor', back_populates='sensor_type')


class Sensor(Base):
    __tablename__ = 'sensors'

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    meta = Column(JSON, nullable=False, server_default=text("'{}'::jsonb"))
    fmt = Column(JSON, nullable=False, server_default=text("'{}'::jsonb"))
    conversion_id = Column(ForeignKey('conversions.id', onupdate='CASCADE'))
    sensor_type_id = Column(ForeignKey('sensor_types.id', onupdate='CASCADE'))
    device_id = Column(ForeignKey('devices.id', onupdate='CASCADE'), nullable=False)

    conversion = relationship('Conversion', back_populates='sensors')
    device = relationship('Device', back_populates='sensors')
    sensor_type = relationship('SensorType', back_populates='sensors')


class Conversion(Base):
    __tablename__ = 'conversions'

    id = Column(UUID, primary_key=True, unique=True, server_default=text("gen_random_uuid()"))
    fun = Column(Text, nullable=False, server_default=text("'id'::text"))

    sensors = relationship('Sensor', back_populates='conversion')

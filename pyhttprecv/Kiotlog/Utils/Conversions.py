UINT16_t_MAX = 65536
INT16_t_MAX = 32768


def float_to_int16(value, max_val, min_val):
    conversion_factor = (INT16_t_MAX - 1) / (max_val - min_val)
    return int(value * conversion_factor)


def float_to_uint16(value, max_val, min_val):
    conversion_factor = (UINT16_t_MAX - 1) / (max_val - min_val)
    return int(value * conversion_factor)


def int16_to_float(value, max_val, min_val):
    conversion_factor = (INT16_t_MAX - 1) / (max_val - min_val)
    return value / conversion_factor


def uint16_to_float(value, max_val, min_val):
    conversion_factor = (UINT16_t_MAX - 1) / (max_val - min_val)
    return value / conversion_factor


def calc_conversion_factor(max_val, min_val, precision):
    from math import log2, ceil

    max_repr = max(abs(max_val / precision), abs(min_val / precision))
    bits = int(ceil(log2(max_repr)))

    conversion_factor = ((1 << bits) - 1) / (max_repr * precision)

    bits = bits + 1 if min_val < 0 else bits

    return bits, conversion_factor


def float_to_intx(value, max_val, min_val, precision):
    bits, conversion_factor = calc_conversion_factor(max_val, min_val, precision)
    return bits, conversion_factor, int(value * conversion_factor)


def intx_to_float(value, max_val, min_val, precision):
    bits, conversion_factor = calc_conversion_factor(max_val, min_val, precision)
    return bits, conversion_factor, value / conversion_factor


def calc_conversion_factorn(bits, max_val, min_val):
    max_repr = 1 << bits if min_val > 0 else 1 << bits - 1
    conversion_factor = (max_repr - 1) / max(abs(max_val), abs(min_val))

    return conversion_factor


def float_to_xintn(value, bits, max_val, min_val):
    conversion_factor = calc_conversion_factorn(bits, max_val, min_val)

    return bits, conversion_factor, int(value * conversion_factor)


def xintn_to_float(value, bits, max_val, min_val):
    conversion_factor = calc_conversion_factorn(bits, max_val, min_val)

    return bits, conversion_factor, round(value / conversion_factor, 1)


def float_to_num(value, bits, precision, max_val, min_val):
    from math import log10

    mul = 1 / precision
    rnd = int(log10(mul) + 1)
    ret = int(round(value - min_val, rnd) * mul)

    return bits, ret


def num_to_float(value, bits, precision, max_val, min_val):
    mul = 1 / precision

    ret = value / mul + min_val

    return bits, ret


def num_by_mul(value, mul, max_val, min_val):
    return value / mul


conversions_map = {
    'id': lambda val, max_val, min_val: val,
    'float_to_int16': lambda val, max_val, min_val: int16_to_float(val, max_val, min_val),
    'float_to_uint16': lambda val, max_val, min_val: uint16_to_float(val, max_val, min_val),
    'x10': lambda val, max_val, min_val: num_by_mul(val, 10, max_val, min_val),
    'x100': lambda val, max_val, min_val: num_by_mul(val, 100, max_val, min_val),
    'x1000': lambda val, max_val, min_val: num_by_mul(val, 1000, max_val, min_val)
}


def convert(value, max_val=1, min_val=0, conversion_type='id'):
    return conversions_map[conversion_type](value, max_val, min_val)


__all__ = ['convert']

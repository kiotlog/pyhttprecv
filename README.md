# Kiotlog Architecture Python Reference Implementation

## PyHttpRecv

### Usage

    Usage: pyhttprecv [OPTIONS]
    
    Options:
      --host TEXT        Bind IP Address (127.0.0.1)
      --port INTEGER     Bind IP Address (5000)
      --pg-user TEXT     Default PostgreSQL user (postgres).
      --pg-pass TEXT     Default PostgreSQL password (postgres).
      --pg-host TEXT     Host of PostgreSQL server (localhost).
      --pg-port INTEGER  Port of PostgreSQL server (5432).
      --pg-db TEXT       Default PostgreSQL database (kiotlog).
      --help             Show this message and exit.

### Prerequisites

- **Python 3.6+**
- **Python Packages**: Please, see `requirements.txt`.

    `$ sudo pip3 install -r requirements.txt`
    
    Please, note that base development tools may be needed in order to install `psycopg2` requirement. Eg. in Alpine Linux, run following command before `pip3`.
    
    `$ sudo apk add --no-cache --update postgresql-dev build-base`

- **Postgres 9.4+**: Database with Kiotlog Reference Schema loaded. (**TODO**: Add schema and setup istructions).

## Running PyHttpRecv

`PyHttpRecv` will establish an HTTP service receiving `application/json` data via `POST` to URL path:

    /<channel>/<application>/devices/<device>/up

where:

- `<channel>` is an identifier for your IoT backend (choose from [`sigfox`, `lorawan`]) - ***mandatory***
- `<application>` is an application identifier inside the IoT backend (eg. *Device Type* in SigFox) - ***mandatory, but not used***
- `<device>` is the device identifier inside the IoT backend (eg. *Device* in SigFox) - ***mandatory***

Please, see [SigFox Callback Setup](#sigfoxsetup) for a full HTTP POST example setup.

### Run from source
Run `PyHttpRecv` from source directory with command:

    $ python3 pyhttprecv
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)

### Docker
#### Run manually

1. Build `PyHttpRecv` image:

        $ docker build -t kiotlog/pyhttprecv .

2. Build Kiotlog DB.

    **TODO**

3. Run image :
    
        $ docker run -d --name kl_postgres kiotlog/postgres
        $ docker run -d -p 5000:5000 --link kl_postgres --name kl_pyhttprecv kiotlog/pyhttprecv --host 0.0.0.0 --pg-host kl_postgres

#### Run with Dockercompose
**TODO**

## Kiotlog DB Setup

### Adding devices to catalog
#### Add device

Add new entries to `devices` table providing at least following columns:

- `device`: Device ID from IoT backend (SigFox or others). **MUST** be unique.
- `meta`: JSON object with information metadata, eg:

        {
            "name": "18B8D6"
            "description": "MKRFOX1200 Roaming"
        }

- `frame`: JSON object with frame setup, eg:

        {
            "bigendian": false,
            "bitfields": false
        }
    
    indicating that device will send data as **packed struct** (set to ` "bitfields": "true"` for **bitfields struct**) in **little endian** order. YMMV. **Default** field is auto-generated with **packed struct** in **big endian** (network) order.
    
    Please, note that unique `id` (UUIDv4) will be auto-generated.
        
#### Add sensors

Add new sensors with `device_id` foreign key set to device `id` generated at previous step. For each sensor provide at least (**MANDATORY**) `meta` and `fmt` fields:

- `meta`: eg. `{"name": "Terrain Temperature"}`
- `conversion_id`: select relevant conversion function from `conversions` table.
- `sensor_type_id`: select sensor type from `sensors_types` table (required by conversion functions for max/min values).

##### Packed Struct Data

- `fmt`: eg. `{"index": 0, "fmt_chr": "h"}` where `index` is the index of sensor's measure
       field inside packed struct and `fmt_chr` is format character as in Python's
       [struct.pack](https://docs.python.org/3/library/struct.html#struct.pack) method
       ([Format Strings](https://docs.python.org/3/library/struct.html#struct-format-strings)).

   **Example**: With C/C++ struct defined as:

        typedef struct __attribute__ ((packed)) sigfox_message {
            uint8_t status;
            int16_t dhtTemperature;
            uint16_t dhtHumidity;
            int16_t moduleTemperature;
            uint8_t lastMessageStatus;
        } SigfoxMessage;


   create five new `sensors` entries with following `meta` and `fmt` (`id` will be auto-generated).


| id | meta | fmt |
| ---|------|---- |
| 43df5a85-d579-4de2-aac8-399258488cab|{"name": "status"}|{"index": 0, "fmt_chr": "B"}|
| 8991ba0d-6263-4a98-ba39-b39b527ba09a|{"name": "temperature"}|{"index": 1, "fmt_chr": "h"} |
| acceb97a-abf7-458e-96ac-800ec3c2722c|{"name": "humidity"}|{"index": 2, "fmt_chr": "H"} |
| cb710293-fa09-4fc6-8e16-db8b8ad5ff2c|{"name": "temperature_interal"}|{"index": 3, "fmt_chr": "h"} |
| c319187d-4d91-4d67-b6e6-f83f13e774c0|{"name": "last"}|{"index": 4, "fmt_chr": "B"} |

##### Bitfields Struct Data
- `fmt`: **TODO**

## SigFox Callback Setup <a name="sigfoxsetup"></a>

1. Create a new Custom Callback and set following parameters:

    - _Type_: _Data_ - _Uplink_
    - _Channel_: _URL_

2. Leave _Custom Paylod Config_ empty and set _Url pattern_ to

    `http://<Server URL>/sigfox/<devicetype id>/devices/{device}/up`

    replacing `<Server URL>` with the URL of your callback service and `<devicetype id>`
with the *id* of your callback Device Type (you can retrieve it from _Information_ tab of _Device Type_ page).

3. Set _Use HTTP Method_ to _POST_.
4. Set _Content type_ to `application/json` and fill _Body_ with following JSON object:

        {
            "app_id": "<devicetype id>",
            "dev_id": "{device}",
            "counter": {seqNumber},
            "is_retry": {duplicate},
            "payload_raw": "{data}",
            "payload_fields": {},
            "metadata": {
                "time": {time},
                "rssi": {rssi},
                "snr": {snr},
                "avg_snr": {avgSnr},
                "station": "{station}",
                "latitude": {lat},
                "longitude": {lng}
            }
        }

    replacing `<devicetype id>` with the *id* of your callback Device Type (see above). Please, see [Minimal MANDATORY Body](#minimalbody) for alternative **minimal** bodies. 

###  Minimal MANDATORY Body <a name="minimalbody"></a>

- With Timestamp from Database INSERT time.

        {
            "payload_raw": "{data}",
        }

- With Timestamp from SigFox BaseStation

        {
            "payload_raw": "{data}",
            "metadata": {
                "time": {time}
            }
        }
 

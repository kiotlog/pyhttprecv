from setuptools import setup, find_packages

setup(
    name='PyHttpRecv',
    version='0.1.0',
    packages=find_packages(),
    url='kiotlog.github.com',
    license='MIT',
    author='Giampaolo Mancini',
    author_email='mancho@trmpln.com',
    description='Python Reference Implementation for Kiotlog IoT Architecture',
    include_package_data=True,
    install_requires=['psycopg2', 'sqlalchemy', 'flask', 'click'],
    entry_points={
        'console_scripts': [
            'pyhttprecv = pyhttprecv.Program:main'
        ]
    }
)

FROM python:alpine as wheeler

COPY requirements.txt /
COPY wheels /wheels
RUN apk add --no-cache --update postgresql-dev build-base
RUN pip wheel -w /wheels -r requirements.txt


FROM python:alpine

COPY --from=wheeler /wheels /wheels
COPY requirements.txt /
RUN apk add --no-cache --update libpq
RUN pip install -f /wheels -r requirements.txt
COPY pyhttprecv pyhttprecv

ENTRYPOINT ["python3", "/pyhttprecv"]